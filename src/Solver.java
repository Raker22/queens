import java.util.ArrayList;
import java.util.Random;

import static java.lang.Math.abs;

public class Solver {
    public int SIZE = 8; // Board size / Number of queens
    public long SEED = 0; // Random seed to use
    public boolean ALLOW_RETRY = true; // Allow retrying after too many iterations
    public int[] stateDescriptions;
    public int queenToMove; // The queen to move next iteration
    public int iterations; // Number of times search is called this attempt
    public int attempts = 0; // Number of times the board is initialized
    public int totalIterations = 0; // Number of times search is called over all attempts
    private final int SCALE = 10; // Run SIZE * SCALE iterations before retrying
    private Random rand; // Use the same random throughout the application

    public static void main(String[] args) {
        // Create solver
        Solver solver = new Solver(args);

        // Initialize board
        solver.initialize();

        System.out.println("Initial Positions");

        solver.printBoard();
        System.out.print("\n");

        System.out.print("Conflicts: " + solver.getNumConflicts());
        System.out.print("\n\n");

        // Find the solution
        System.out .println("Solving...");
        solver.solve();

        System.out.println("\nFinal Positions");

        solver.printBoard();
        System.out.print("\n");

        System.out.println("Conflicts: " + solver.getNumConflicts());
        System.out.println("Iterations: " + solver.iterations);
        System.out.println("Attempts: " + solver.attempts);
        System.out.println("Total Iterations: " + solver.totalIterations);
    }

    Solver(String[] args) {
        // Parse args
        if (args.length >= 1) {
            SIZE = Integer.parseInt(args[0]);

            if (args.length >= 2) {
                SEED = Long.parseLong(args[1]);

                if (args.length >= 3) {
                    ALLOW_RETRY = Boolean.parseBoolean(args[2]);
                }
            }
        }

        this.rand = new Random(SEED);
        this.stateDescriptions = new int[SIZE];
    }

    public void solve() {
        // Solve the N Queens problem
        while (!this.isFinalState()) {
            // Keep searching until we find the solution
            if (this.iterations > SIZE * SCALE && ALLOW_RETRY) {
                // Retry since we had too many attempts
                this.totalIterations += this.iterations;
                this.initialize();
            }
            else {
                // Search for solution
                this.search();
            }
        }

        this.totalIterations += this.iterations;
    }

    public void initialize() {
        // Start first queen in random row and add all other queens to a
        // random row with the least number of conflicts
        System.out.println("Initializing...\n");

        this.iterations = 0;
        this.attempts++;
        this.stateDescriptions[0] = this.rand.nextInt(SIZE);

        for (int i = 1; i < SIZE; i++) {
            ArrayList<Conflict> minConflicts = this.getMinConflicts(i, i);
            int index = this.rand.nextInt(minConflicts.size());
            this.stateDescriptions[i] = minConflicts.get(index).row;
        }

        ArrayList<Conflict> conflicts = this.getMaxConflicts();

        if (conflicts.size() != 0) {
            // Only choose a starting queen if there is a conflict
            Conflict startingConflict = conflicts.get(this.rand.nextInt(conflicts.size()));
            this.queenToMove = startingConflict.col;
        }
    }

    public void search() {
        // Move the queenToMove to one of the rows with the least conflicts
        ArrayList<Conflict> minConflicts = this.getMinConflicts(this.queenToMove);
        Conflict conflict = minConflicts.get(this.rand.nextInt(minConflicts.size()));

        System.out.println("Moving Queen " + this.queenToMove + " from "
                + this.stateDescriptions[this.queenToMove] + " to " + conflict.row);

        // Move the queen to the selected row
        this.stateDescriptions[this.queenToMove] = conflict.row;

        if (conflict.getNum() != 0) {
            // Get one of the queens that the moved queen now conflicts with
            this.queenToMove = conflict.queens.get(this.rand.nextInt(conflict.getNum()));
        }
        else {
            // The space the queen moved to had no conflicts
            // Get one of the queens that has the most conflicts with other queens
            ArrayList<Conflict> maxConflicts = this.getMaxConflicts();

            if (maxConflicts.size() != 0) {
                // Only choose a next queen if there are conflicts left
                conflict = maxConflicts.get(this.rand.nextInt(maxConflicts.size()));
                this.queenToMove = conflict.col;
            }
        }

        this.iterations++;
    }

    public Conflict getConflictAt(int row, int col, int maxCol) {
        // Get the queens that conflict on space (row,col)
        // Only check queens in columns from 0 inclusive to maxCol exclusive
        // We need these functions with maxCol so that we don't check uninitialized columns
        Conflict conflict = new Conflict(row, col);

        for (int checkCol = 0; checkCol < maxCol; checkCol++) {
            // Find conflicts from queens in the other columns
            if (checkCol != col) {
                // Don't count self as conflict
                int checkRow = this.stateDescriptions[checkCol];
                if (checkRow == row || abs(checkRow - row) == abs(checkCol - col)) {
                    // If the queen is in the same row on on the diagonal there is a conflict
                    conflict.addQueen(checkCol);
                }
            }
        }

        return conflict;
    }

    public Conflict getConflictAt(int row, int col) {
        // Get the queens that conflict on space (row,col)
        return this.getConflictAt(row, col, SIZE);
    }

    public ArrayList<Conflict> getMinConflicts(int col, int maxCol) {
        // Get the rows with the minimum number of conflicts
        // Only check columns [0,maxCol)
        ArrayList<Conflict> rows = new ArrayList<>();

        // The first row is automatically has the min number of conflicts
        Conflict conflict = this.getConflictAt(0, col, maxCol);
        int min = conflict.getNum();
        rows.add(conflict);

        for (int i = 1; i < SIZE; i++) {
            // Add all rows with the min number of conflicts to the returned list
            conflict = this.getConflictAt(i, col, maxCol);
            int numConflicts = conflict.getNum();

            if (numConflicts == min) {
                // Add all rows with min number of conflicts
                rows.add(conflict);
            }
            else if (numConflicts < min) {
                // If we find a new min clear the list
                rows.clear();
                min = numConflicts;
                rows.add(conflict);
            }
        }

        return rows;
    }

    public ArrayList<Conflict> getMinConflicts(int col) {
        // Get the rows with the minimum number of conflicts
        return this .getMinConflicts(col, SIZE);
    }

    public ArrayList<Conflict> getConflicts() {
        // Get conflicts for all the queens
        ArrayList<Conflict> conflicts = new ArrayList<>();

        for (int col = 0; col < SIZE; col++) {
            // Get the total conflicts for each queen
            int row = this.stateDescriptions[col];
            Conflict conflict = this.getConflictAt(row, col);

            if (conflict.getNum() != 0) {
                conflicts.add(this.getConflictAt(row, col));
            }
        }

        // Divide conflicts by 2 since each conflict is counted twice
        // If one queen can attack another then it can also be attacked by the other
        return conflicts;
    }

    public ArrayList<Conflict> getMaxConflicts() {
        // Get the conflicts that conflict with the most queens
        ArrayList<Conflict> conflicts = this.getConflicts();
        ArrayList<Conflict> maxConflicts = new ArrayList<>();
        if (conflicts.size() != 0) {
            Conflict conflict = conflicts.get(0);
            int max = conflict.getNum();

            maxConflicts.add(conflict);

            for (int i = 1; i < conflicts.size(); i++) {
                // Add all conflicts with the max number of queens to the return list
                conflict = conflicts.get(i);
                int numConflicts = conflict.getNum();

                if (numConflicts == max) {
                    // If conflicts with the same number of queens add it to the list
                    maxConflicts.add(conflict);
                } else if (numConflicts > max) {
                    // If it conflicts with more queens clear the list and reset max
                    maxConflicts.clear();
                    max = numConflicts;
                    maxConflicts.add(conflict);

                }
            }
        }

        return maxConflicts;
    }

    public int getNumConflicts() {
        // Find the number of conflicts on the board
        int numConflicts = 0;

        for (Conflict conflict : this.getConflicts()) {
            // Count the number of conflicts on the board
            numConflicts += conflict.getNum();
        }

        // Each conflict consists of two queens so it is reported twice
        // Divide by two to get the number of actual conflicts
        return numConflicts / 2;
    }

    public boolean isFinalState() {
        // Check if there are any conflicts
        return this.getNumConflicts() == 0;
    }

    public void printBoard() {
        // Print the board for visual debugging
        for (int i : this.stateDescriptions) {
            System.out.print(i + " ");
        }
        System.out.print("\n");

        for (int r = 0; r < SIZE; r++) {
            for (int c = 0; c < SIZE; c++) {
                if (this.stateDescriptions[c] == r) {
                    System.out.print("Q");
                }
                else {
                    System.out.print("-");
                }
                System.out.print(" ");
            }

            System.out.print("\n");
        }
    }
}
