import java.util.ArrayList;

public class Conflict {
    public int row; // The row the conflict is associated with
    public int col; // The column (queen) the conflicts are associated with
    public ArrayList<Integer> queens = new ArrayList<>(); // Queen (column) conflicting with (row,col)

    public Conflict(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public int getNum() {
        // Get the number of queens this row conflicts with
        return this.queens.size();
    }

    public void addQueen(int col) {
        // Add a conflicting queen
        this.queens.add(col);
    }

    @Override
    public String toString() {
        String result = "Conflict(row: " + this.row + ", col: " + this.col + ", queens: [";

        for (Integer queen : this.queens) {
            result += queen + ", ";
        }

        return result.substring(0, result.length() - 2) + "])";
    }
}
